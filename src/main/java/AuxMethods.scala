package main.java

import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.sql.types.{DoubleType, NumericType, StringType}
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * Created by dan on 20.04.2017.
  */
object AuxMethods {

  def normalize(dfP : DataFrame , dfN : DataFrame) : (DataFrame, DataFrame) = {
    val df = dfP.union(dfN)


    val cols = df.columns.filter(!_.equals("BAG_ID"))

    val dfMins = df.groupBy().min(cols: _*).toDF(cols:_*)
    val dfMaxs = df.groupBy().max(cols: _*).toDF(cols:_*)

    val rowMins = dfMins.first()
    val rowMaxs = dfMaxs.first()



    var rezDFP = dfP

    cols.foreach(c => {
      rezDFP = rezDFP.withColumn(c, (rezDFP(c) - rowMins.getAs[Double](c)) /
        (rowMaxs.getAs[Double](c) - rowMins.getAs[Double](c)))

    })

    var rezDFN = dfN

    cols.foreach(c => {
      rezDFN = rezDFN.withColumn(c, (rezDFN(c) - rowMins.getAs[Double](c)) /
        (rowMaxs.getAs[Double](c) - rowMins.getAs[Double](c)))

    })


    (rezDFP,  rezDFN)
  }

  def readAndPrepareDF(filePath : String,
                       pathToIndexerReference : String) : DataFrame = {


    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    val rootLogger = Logger.getRootLogger()
    rootLogger.setLevel(Level.ERROR)

    import spark.implicits._

    // The two DataFrames must have the same headers!
    var df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .csv(filePath)

    var stringColumns : Array[String] = Array[String]()
    var numericColumns : Array[String] = Array[String]()

    // Set -> do not allow duplicates
    var dropColumns : Set[String] = Set[String]()

    for (c <- df.schema.fields)
    // Get the string columns
      c.dataType match {
        case _ : StringType => stringColumns = stringColumns :+ c.name
        case _ : NumericType => numericColumns = numericColumns :+ c.name
        case _ => dropColumns = dropColumns + c.name
      }

    // Reread the df, this time without inferring the schema
    df = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .csv(filePath)


    // Make sure there are no rows containing nulls
    //df = df.na.drop()

    // Read the file where both the negative and positive DFs are included
    val indexerReference = spark.read
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .csv(pathToIndexerReference)

    // Index the string columns
    for (columnName <- stringColumns) {
      //println(columnName)

      val indexer = new StringIndexer()
        .setInputCol(columnName)
        .setOutputCol(columnName + "Indexed")

      try {
        df = indexer
          .fit(indexerReference)
          .transform(df)
          .drop(columnName)
          .withColumnRenamed(columnName + "Indexed", columnName)
      } catch {
        case _ : NullPointerException =>
          def replaceNull() : Double = -1.0

          val replaceNullUDF = org.apache.spark.sql.functions.udf(replaceNull _)

          df = df.withColumn(columnName, replaceNullUDF())

        //dropColumns = dropColumns + columnName
      }
    }

    for (columnName <- numericColumns)
      df = df
        .withColumn(columnName, df(columnName).cast(DoubleType))


    // Remove the drop columns
    df = df.drop(dropColumns.toSeq : _*)

    // Remove the rows containing null somewhere in them, and return the result
    df.na.drop()
  }


}
