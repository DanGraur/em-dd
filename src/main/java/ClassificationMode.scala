package main.java

/**
  * Enumeration used for selecting the classification mode for the EMDD algorithm
  *
  * MAX - the highest value (NLDD) hypothesis
  * MIN - the lowest value (NLDD) hypothesis
  * AVG - use all the hypothesis and average their outputs
  *
  * Created by dan on 22.04.2017.
  */
object ClassificationMode extends Enumeration {
  type ClassificationMode = Value

  val MAX, AVG, MIN = Value
}
