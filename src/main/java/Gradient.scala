package main.java

import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

import scala.collection.mutable

/**
  * Designed to be applied on a DF after an empty group by. Will compute the mean of the weighted distance,
  * which will then be used as the exponent for e, and finally it will be plugged in a log function, based on
  * the instance's bag label. All of this on a SINGULAR ROW --> only used to enforce Distribution in SPARK
  *
  * Created by dan on 17.04.2017.
  */
class Gradient(structTypeParam : StructType,
               indexMapperParam : Map[Int, String],
               targetParam : Array[Double],
               scalesParam : Array[Double]) extends UserDefinedAggregateFunction {

  private val indexMapper : Map[Int, String] = indexMapperParam
  private val target : Array[Double] = targetParam
  private val scales : Array[Double] = scalesParam

  override def inputSchema: StructType = structTypeParam

  override def bufferSchema: StructType =
    StructType(
      Seq[StructField](
        StructField(
          "gradient",
          ArrayType(DoubleType)
        )
      )
    )


  // Return the gradient as an array of Doubles
  override def dataType: DataType = ArrayType(DoubleType)

  // Always
  override def deterministic: Boolean = true

  // Initialize the gradient to 0.0 for each feature
  override def initialize(buffer: MutableAggregationBuffer): Unit = {

    buffer(0) = Array.fill(target.length + scales.length){0.0}

  }

  // Main computation - the value in the buffer will be overwritten by the new value
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {

    val partialGradient : mutable.WrappedArray[Double] = buffer(0).asInstanceOf[mutable.WrappedArray[Double]]

    // If the label of the instance's bag is positive
    if (input.getDouble(inputSchema.fieldIndex("LABEL")) == 1.0)
      for (index <- target.indices) {
        // Compute the gradient
        partialGradient(index) -=
          (2.0 / target.length.toDouble) * (scales(index) * scales(index)) * (input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index))

        partialGradient(index + target.length) +=
          (2.0 / target.length.toDouble) * scales(index) * Math.pow(input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index), 2.0)

        /* println(
           "Gradient: " +
           ((2 / target.length.toDouble) * (scales(index) * scales(index)) * (input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index))) +
           "\nScale Gradient: " +
           ((2 / target.length.toDouble) * scales(index) * Math.pow(input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index), 2.0))
         )*/
      }
    // If the label of the instance's bag is negative
    else {
      // Compute the instance's probability
      var probability : Double = 0.0

      for (index <- target.indices) {

        probability += Math.pow(input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index), 2.0)  *
          scales(index) *
          scales(index)

      }



      // probability

      //AICI CRED CA AR TREBUI SA FIE probability = Math.exp(-probability / target.length) / (1.0 - Math.exp(-probability / target.length))
      //http://www.wolframalpha.com/input/?i=d%2Fdx+(+++-log(+1-+e%5E(++-(+++s1*s1*(x-h1)*(x-h1)+%2B+s2*s2*(y-h2)*(y-h2)+++++)%2F(N)++)+++)++++)
      //probability = 1.0 / (1.0 - Math.exp(-probability / target.length))
      probability = Math.exp(-probability / target.length) / (1.0 - Math.exp(-probability / target.length))

      for (index <- target.indices) {
        // Compute the gradient
        partialGradient(index) += probability * (2.0 / target.length.toDouble) * (scales(index) * scales(index)) * (input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index))
        partialGradient(index + target.length) -= probability * (2.0 / target.length.toDouble) * scales(index) * Math.pow(input.getDouble(inputSchema.fieldIndex(indexMapper(index))) - target(index), 2.0)
      }

    }

    buffer(0) = partialGradient

  }

  //Merge two gradients together by adding their respective entries
  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {

    /*
        buffer1(0).asInstanceOf[mutable.WrappedArray[Double]].foreach(x => println("Gradient1: " + x))
        buffer2(0).asInstanceOf[mutable.WrappedArray[Double]].foreach(x => println("Gradient2: " + x))


        println("---------------**********---------------")
    */

    val partialGradientOne : mutable.WrappedArray[Double] = buffer1(0).asInstanceOf[mutable.WrappedArray[Double]]
    val partialGradientTwo : mutable.WrappedArray[Double] = buffer2(0).asInstanceOf[mutable.WrappedArray[Double]]

    buffer1(0) = partialGradientOne.zip(partialGradientTwo).map(x => x._1 + x._2)
  }

  // Return the final gradient, i.e. the wrapped array containing the gradient
  override def evaluate(buffer: Row): Any = {
    buffer(0)
  }
}