package main.java

import breeze.linalg.DenseVector
import breeze.optimize.{DiffFunction, LBFGS}
import java.io.{File, PrintWriter}

import main.java.ClassificationMode.ClassificationMode
import org.apache.commons.math3.analysis.{MultivariateFunction, MultivariateVectorFunction}
import org.apache.commons.math3.optim._
import org.apache.commons.math3.optim.nonlinear.scalar.{GoalType, ObjectiveFunction, ObjectiveFunctionGradient}
import org.apache.commons.math3.optim.nonlinear.scalar.gradient.NonLinearConjugateGradientOptimizer
import org.apache.spark.sql.{DataFrame, Row, SparkSession}

import scala.collection.mutable
import org.apache.spark.sql.functions.{lit, max}
import org.apache.spark.sql.functions._

import scala.io.Source
import scala.util.Random

/**
  * Created by dan on 17.04.2017.
  */
object EMDD {

  def main(args: Array[String]): Unit = {

    if (args.length != 6) {

      println("Too few Arguments!\n" +
        "Usage: SequentialAPR-1.0-SNAPSHOT-jar-with-dependencies.jar " +
        "POSITIVE_CSV_NAME NEGATIVE_CSV_NAME STRING_INDEXER_REFERENCE " +
        "OUTPUT_FILE BAG_ID_COLUMN_NAME PATH_TO_REL_FEATURES <RETURN>")

      System.exit(0xFF)
    }

    val spark = SparkSession
      .builder()
      .appName("SparkSessionZipsExample")
      .getOrCreate()

    import spark.implicits._

    spark.sparkContext.setLogLevel("FATAL")

    val indexMapper : Map[Int, String] = getIndexMapper(args(5))

    val result = EMDD(args(0), args(1), indexMapper, args(4), totalRuns = 5)

    printResult(result)
    writeResultToFile(result, args(3))

    //val result = readResultFromFile(args(3))

    val classificationThreshold = computeBestThreshold(
      result,
      indexMapper,
      args(0),
      args(1),
      ClassificationMode.MIN,
      ClassificationMode.MIN,
      args(4)
    )

    println("The threshold: " + classificationThreshold)

    val (posClassification, negClassification) = EMDDClassify(
      result,
      indexMapper,
      classificationThreshold,
      args(0),
      args(1),
      ClassificationMode.MIN,
      args(4)
    )

    println("The number of true positives: " + posClassification.where(posClassification.col("PREDICTION").equalTo(1.0)).count())
    println("The number of false negatives: " + posClassification.where(posClassification.col("PREDICTION").equalTo(0.0)).count())
    println("The number of true negatives: " + negClassification.where(negClassification.col("PREDICTION").equalTo(0.0)).count())
    println("The number of false positives: " + negClassification.where(negClassification.col("PREDICTION").equalTo(1.0)).count())

  }


  /**
    * Run the EMDD algorithm
    *
    * @param dfPosPath the path to the training DF containing the positive bags
    * @param dfNegPath the path to the training DF containing the negative bags
    * @param indexMapper the map between the target and the DF's header
    * @param bagIdColumnName the name of the column hosting the bag id
    * @param convergenceTolerance the convergence criteria (default: 0.1)
    * @param totalRuns the number of total runs
    * @return
    */
  def EMDD(dfPosPath : String,
           dfNegPath : String,
           indexMapper : Map[Int, String],
           bagIdColumnName : String,
           convergenceTolerance : Double = 0.1,
           totalRuns : Int = 10) : Array[(Array[Double], Array[Double], Double)] = {

    // Pre-process the data - START -
    var (dfPos, dfNeg) = AuxMethods.normalize(
      AuxMethods.readAndPrepareDF(
        dfPosPath,
        dfPosPath
      ),
      AuxMethods.readAndPrepareDF(
        dfNegPath,
        dfNegPath
      )
    )

    dfPos = dfPos
      .withColumn("LABEL", lit(1.0))

    dfNeg = dfNeg
      .withColumn("LABEL", lit(0.0))

    // Pre-process the data - END -

    // Construct the training DF
    val trainingDF = dfPos.union(dfNeg)

    // Init the results
    var results : Array[(Array[Double], Array[Double], Double)] = Array[(Array[Double], Array[Double], Double)]()

    // Zip with index so as to randomly choose starting positive instances
    val dfPosIndexed = dfPos.rdd.zipWithIndex()

    // Set which contains the indexes of the chosen instances
    var chosenStartingInstances : Set[Long] = Set[Long]()

    // Get the total number of positive instances
    val totalPositiveInstances : Long = dfPos.count()

    // Random number generating object
    val numberGenerator : Random = new Random()

    for (_ <- 0 until totalRuns) {
      // Choose a random starting instance
      var randomIndex = Math.abs(numberGenerator.nextLong()) % totalPositiveInstances

      while(chosenStartingInstances contains randomIndex)
        randomIndex = Math.abs(numberGenerator.nextLong()) % totalPositiveInstances

      // Add the new index
      chosenStartingInstances = chosenStartingInstances + randomIndex

      // Get the starting row
      val chosenRow : Row = dfPosIndexed
        .filter(tuple => tuple._2 == randomIndex)
        .first()
        ._1


      // Transform the row to target
      val hypothesis : Array[Double] = Array.fill(indexMapper.size){0.5}
      val scales : Array[Double] = Array.fill(indexMapper.size){1.0}

      for (mapperElement <- indexMapper)
        hypothesis(mapperElement._1) = chosenRow.getAs[Double](mapperElement._2)

      //hypothesis.foreach(x => println("Target Element: " + x))

      // Compute a new result
      results = results :+ EMDDIteration(trainingDF, convergenceTolerance, hypothesis, scales, indexMapper, bagIdColumnName)
    }

    // Return the results
    results
  }


  /**
    * Perform an iteration of the EM-DD algorithm
    *
    * @param df the initial DF containing the training bags
    * @param convergenceTolerance the tolerance in terms of convergence between two consecutively generated hypothesis
    * @param hypothesis the initial hypothesis
    * @param scales the scales vector
    * @param indexMapper the map between the hypothesis and the DF's header
    * @param bagIdColumnName the name of the column hosting the bag id
    * @return a tuple consisting of the hypothesis and the feature scales
    */
  def EMDDIteration(df : DataFrame,
                    convergenceTolerance : Double,
                    hypothesis : Array[Double],
                    scales : Array[Double],
                    indexMapper : Map[Int, String],
                    bagIdColumnName : String) : (Array[Double], Array[Double], Double) = {

    // Check that the vector lengths are equal
    if (hypothesis.length != scales.length)
      throw new IllegalArgumentException("The length of the hypothesis vector and the scales vector must be equal!")

    // Move to var so as to allow changes
    var target = hypothesis ++ scales

    // Initialize the control variables
    var NLDDDifference = Double.MaxValue
    var previousNLDD = Double.MaxValue
    var finalNLDD = 0.0

    do {

      // The E step
      val selectedInstances = computeMaxPerBag(
        df,
        target,
        indexMapper,
        bagIdColumnName
      )

      /*// The M step
      val targetAndValue = minimizeTarget(
        target,
        selectedInstances,
        indexMapper
      )

      // Store the new target
      target = targetAndValue.getPoint
      // Get the new target's NLDD value
      finalNLDD = targetAndValue.getValue*/


      // The M step
      val targetAndValue = minimizeTargetBFGS(
        target,
        selectedInstances,
        indexMapper
      )

      // Store the new target
      target = targetAndValue._1
      // Get the new target's NLDD value
      finalNLDD = targetAndValue._2




      // Calculate the NLDDDifference
      NLDDDifference = previousNLDD - finalNLDD
      // Update the previous NLDD value
      previousNLDD = finalNLDD

    } while(NLDDDifference > convergenceTolerance)

    // Return the tuple
    (target take indexMapper.size, target drop indexMapper.size, finalNLDD)
  }

  /**
    * Determine the max probability instance for each bag, relative to the current hypothesis. The E step in EMDD.
    *
    * @param df the DF containing the bags
    * @param target the current hypothesis + the feature scales
    * @param indexMapper maps between an index in the target and a column; Moreover, its length will represent
    *                    the number of features (it is considered that target is featureNumber * 2)
    * @return a DF containing the selected instances from each bag
    */
  def computeMaxPerBag(df: DataFrame,
                       target: Array[Double],
                       indexMapper: Map[Int, String],
                       bagIdColumnName : String): DataFrame = {

    val nrFeatures = indexMapper.size

    val featureMap = Map(df.columns.map(c => c -> df.columns.indexOf(c)): _*)

    val targetConceptTuple = target.splitAt(nrFeatures)
    val targetConcept = targetConceptTuple._1.zip(targetConceptTuple._2).zipWithIndex


    val probabilityUDF = udf((features: Seq[Double]) => {

      val sum = targetConcept.map(conceptFeature => {
        val conceptIndex = conceptFeature._2

        val conceptValue = conceptFeature._1._1
        val conceptScale = conceptFeature._1._2

        Math.pow(conceptScale * (conceptValue - features(featureMap(indexMapper(conceptIndex)))), 2)
      }).sum

      Math.pow(Math.E, -sum / nrFeatures)


    })

    var dfWithProbability = df.withColumn("PROBABILITY", probabilityUDF(array(df.columns.map(df(_)): _*)))

    //dfWithProbability.select("PROBABILITY").collect.foreach(println _)


    val dfWithProbabilityMaxPerBag = dfWithProbability.groupBy(bagIdColumnName).agg(max(
      struct(("PROBABILITY" +: dfWithProbability.columns.filter(!_.equals("PROBABILITY"))).map(dfWithProbability(_)): _*)
    ).alias("agg")
    )

    val initialColumns = df.columns.filter(!_.equals(bagIdColumnName))

    dfWithProbabilityMaxPerBag.select(dfWithProbabilityMaxPerBag(bagIdColumnName) +: initialColumns.map(c => dfWithProbabilityMaxPerBag("agg")(c).alias(c)): _*)

    // dfWithProbabilityMaxPerBag.printSchema()
    //dfWithProbabilityMaxPerBag

  }

  /**
    * Computes the NLDD metric
    *
    * @param selectedInstances the instances selected in the E Step; it is assumed that these instances have a column which holds
    *                          their bag's label
    * @param indexMapper maps between an index in the data and a column; Moreover, its length will represent
    *                    the number of features (it is considered that data is featureNumber * 2)
    * @param data the hypothesis + the scales, as represented by an array of doubles
    * @return the NLDD
    */
  def computeDensity(selectedInstances : DataFrame,
                     indexMapper : Map[Int, String])
                    (data : Array[Double]) : Double ={
    // Get the hypothesis and the scale vectors
    val (target, scales) = data.splitAt(indexMapper.size)

    //val ddAggregator : LogDD = new LogDD(selectedInstances.schema, indexMapper, target, scales)

    selectedInstances.rdd.map(row => {

      var temp : Double = 0.0

      for (index <- target.indices) {

        temp += Math.pow(row.getAs[Double](indexMapper(index)) - target(index), 2.0)  *
          scales(index) *
          scales(index)

      }

      // Average
      temp = Math.exp(-temp / target.length)

      //println("Probability: " + temp)

      // If the instance's bag is positive
      if (row.getAs[Double]("LABEL") == 1.0) {
        if (temp == 0.0)
          -Math.log(1.0e-10)
        else
          -Math.log(temp)
      } else {
        if (temp == 1.0)
          -Math.log(1.0e-10)
        else
          -Math.log(1.0 - temp)
      }

    }).sum()

  }

  /**
    * Compute the gradient of the NLDD
    *
    * @param selectedInstances the instances selected in the E Step; it is assumed that these instances have a column which holds
    *                          their bag's label
    * @param indexMapper maps between an index in the data and a column; Moreover, its length will represent
    *                    the number of features (it is considered that data is featureNumber * 2)
    * @param data the hypothesis + the scales, as represented by an array of doubles
    * @return the gradient
    */
  def computeGradient(selectedInstances : DataFrame,
                      indexMapper : Map[Int, String])
                     (data : Array[Double]) : Array[Double] = {

    // Total features
    val featureTotal = indexMapper.size

    // Get the hypothesis and the scale vectors
    val (target, scales) = data.splitAt(featureTotal)

    val gradientCalculator : Gradient = new Gradient(
      selectedInstances.schema,
      indexMapper,
      target,
      scales
    )

    selectedInstances
      .groupBy()
      .agg(gradientCalculator(selectedInstances.columns.map(selectedInstances(_)) : _*).as("GRADIENT"))
      .first()
      .getAs[mutable.WrappedArray[Double]]("GRADIENT")
      .toArray
  }

  /**
    * Generate a new hypothesis based on a set of selected instances and the previous hypothesis. The M step in the EMDD.
    *
    * @param currentHypothesis the current hypothesis + feature scales
    * @param selectedInstances the DF containing the selected instances (from the E step)
    * @param indexMapper the map between the hypothesis and the DF's header
    * @return a PointValuePair containing the new hypothesis + the new scales, and the value of the minimized NLDD
    */
  def minimizeTarget(currentHypothesis : Array[Double],
                     selectedInstances : DataFrame,
                     indexMapper : Map[Int, String]) : PointValuePair = {

    val optimizer : NonLinearConjugateGradientOptimizer =
      new NonLinearConjugateGradientOptimizer(NonLinearConjugateGradientOptimizer.Formula.FLETCHER_REEVES, new SimpleValueChecker(0.0001, -0.0001))

    // Create the NLDD and the Gradient functions

    val density = new MultivariateFunction {
      override def value(doubles: Array[Double]): Double = {
        computeDensity(selectedInstances, indexMapper)(doubles)
      }
    }

    val gradient = new MultivariateVectorFunction {
      override def value(doubles: Array[Double]): Array[Double] = {
        computeGradient(selectedInstances, indexMapper)(doubles)
      }
    }

    optimizer.optimize(
      GoalType.MINIMIZE,
      new ObjectiveFunction(density),
      new ObjectiveFunctionGradient(gradient),
      new InitialGuess(currentHypothesis),
      new MaxIter(1000),
      new MaxEval(50000)
    )
  }


  /**
    * Attempts to find the minimal point of a function (NLDD) and its value in that function (NLDD) by using the BFGS algorithm
    *
    * @param currentHypothesis the current hypothesis + scales
    * @param selectedInstances the selected instances from the E Step
    * @param indexMapper the map between the hypothesis and the DF's header
    * @return the minimal point of the NLDD function and its NLDD value
    */
  def minimizeTargetBFGS(currentHypothesis : Array[Double],
                         selectedInstances : DataFrame,
                         indexMapper : Map[Int, String]) : (Array[Double], Double) = {

    val f = new DiffFunction[DenseVector[Double]] {
      override def calculate(x: DenseVector[Double]): (Double, DenseVector[Double]) = {
        // (VALUE, GRADIENT)
        (computeDensity(selectedInstances, indexMapper)(x.toArray), DenseVector(computeGradient(selectedInstances, indexMapper)(x.toArray)))

      }
    }

    val lbfgs = new LBFGS[DenseVector[Double]](maxIter=100, m=5, tolerance = 1.0e-6)


    val minPoint = lbfgs.minimize(f,DenseVector(currentHypothesis))

    (minPoint.toArray, f.valueAt(minPoint))
  }






  /**
    * Compute the instance probability
    *
    * @param results the array containing the results of the EMDD algorithm (one entry for each run)
    * @param indexMapper the map between the hypothesis and the DF's header
    * @param row the instance being classified
    * @return the probability of the instance
    */
  def EMDDInstanceProbability(results : Array[(Array[Double], Array[Double], Double)],
                              indexMapper : Map[Int, String])
                              (row : Row) : Double = {

    var rowProbability : Double = 0.0

    for ((target, scales, _) <- results) {

      var resultProbability : Double = 0.0

      for (index <- target.indices) {

        resultProbability += Math.pow(row.getAs[Double](indexMapper(index)) - target(index), 2.0) *
          scales(index) *
          scales(index)

      }

      // Average and add to row probability
      rowProbability += Math.exp(-resultProbability / target.length)
    }

    //println("*The row probability: " + (rowProbability / results.length))

    // Return the probability
    rowProbability / results.length
  }


  /**
    * Perform per instance classification for the EMDD algorithm
    *
    * @param results the array containing the results of the EMDD algorithm (one entry for each run)
    * @param indexMapper the map between the hypothesis and the DF's header
    * @param row the instance being classified
    * @return the classification of the instance
    */
  def EMDDInstanceClassify(results : Array[(Array[Double], Array[Double], Double)],
                           indexMapper : Map[Int, String],
                           threshold : Double)
                          (row : Row) : Double = {

    var rowProbability : Double = 0.0

    for ((target, scales, _) <- results) {

      var resultProbability : Double = 0.0

      for (index <- target.indices) {

        resultProbability += Math.pow(row.getAs[Double](indexMapper(index)) - target(index), 2.0) *
          scales(index) *
          scales(index)

      }

      // Average and add to row probability
      rowProbability += Math.exp(-resultProbability / target.length)
    }

    //println("*The row probability: " + (rowProbability / results.length))

    // Return the classification
    if ((rowProbability / results.length) >= threshold)
      1.0
    else
      0.0
  }

  /**
    * Compute a guess for the best acceptable threshold for classification
    *
    * @param results the results of the EMDD algorithm
    * @param indexMapper the map between the hypothesis and the DF's header
    * @param positivePath the path to the DF hosting the positive bags for testing
    * @param negativePath the path to the DF hosting the negative bags for testing
    * @param classificationMode the mode used for the classification operation (to be made). May be: MIN, AVG or MAX
    * @param probabilityMode the mode used for determining the 'best' threshold. May be: MIN, AVG or MAX
    * @param bagIdColumnName the name of the column hosting the bag id
    * @return
    */
  def computeBestThreshold(results : Array[(Array[Double], Array[Double], Double)],
                           indexMapper : Map[Int, String],
                           positivePath : String,
                           negativePath : String,
                           classificationMode : ClassificationMode,
                           probabilityMode: ClassificationMode,
                           bagIdColumnName : String) : Double = {

    // Read the DFs that need to be classified (and normalize them)
    val (dfPos, _) = AuxMethods.normalize(
      AuxMethods.readAndPrepareDF(
        positivePath,
        positivePath
      ),
      AuxMethods.readAndPrepareDF(
        negativePath,
        negativePath
      )
    )

    val probabilityCalculator = classificationMode match {
      // Choose the greatest NLDD valued hypothesis
      case ClassificationMode.MAX =>
        udf(EMDDInstanceProbability(Array(results.maxBy(_._3)), indexMapper) _)
      case ClassificationMode.AVG =>
        // Average the probabilities of the hypotheses
        udf(EMDDInstanceProbability(results, indexMapper) _)
      case _ =>
        // Choose the smallest NLDD valued hypothesis
        udf(EMDDInstanceProbability(Array(results.minBy(_._3)), indexMapper) _)
    }

    probabilityMode match {
        // Average the maximal bag probabilities to get the threshold
      case ClassificationMode.AVG =>
        dfPos
          .withColumn("PROBABILITY", probabilityCalculator(struct(dfPos.columns.map(dfPos(_)): _*)))
          .groupBy(bagIdColumnName)
          .agg(max("PROBABILITY").as("MAX_PROBABILITY"))
          .groupBy()
          .agg(mean("MAX_PROBABILITY").as("MEAN_MAX_PROBABILITY"))
          .first()
          .getAs[Double]("MEAN_MAX_PROBABILITY")
        // Get the min(max(bag_probability)) as threshold
      case ClassificationMode.MIN =>
        dfPos
          .withColumn("PROBABILITY", probabilityCalculator(struct(dfPos.columns.map(dfPos(_)): _*)))
          .groupBy(bagIdColumnName)
          .agg(max("PROBABILITY").as("MAX_PROBABILITY"))
          .groupBy()
          .agg(min("MAX_PROBABILITY").as("MIN_MAX_PROBABILITY"))
          .first()
          .getAs[Double]("MIN_MAX_PROBABILITY")
        // Get the maximal probability as threshold
      case _ =>
        dfPos
          .withColumn("PROBABILITY", probabilityCalculator(struct(dfPos.columns.map(dfPos(_)): _*)))
          .groupBy()
          .agg(max("PROBABILITY").as("MAX_PROBABILITY"))
          .first()
          .getAs[Double]("MAX_PROBABILITY")
    }

  }


  /**
    * Perform per bag classification for the EMDD algorithm
    *
    * @param results the results of the EMDD algorithm
    * @param indexMapper the map between the hypothesis and the DF's header
    * @param threshold the threshold above which the instance will be classified as positive
    * @param positivePath the path to the DF hosting the positive bags for training
    * @param negativePath the path to the DF hosting the negative bags for testing
    * @param mode the mode used for the classification operation. May be: MIN, AVG or MAX
    * @param bagIdColumnName the name of the column hosting the bag id
    * @return a tuple containing the positive DF, respectively the negative DF, both classified
    */
  def EMDDClassify(results : Array[(Array[Double], Array[Double], Double)],
                   indexMapper : Map[Int, String],
                   threshold : Double,
                   positivePath : String,
                   negativePath : String,
                   mode : ClassificationMode,
                   bagIdColumnName : String) : (DataFrame, DataFrame) = {

    // Read the DFs that need to be classified (and normalize them)
    val (dfPos, dfNeg) = AuxMethods.normalize(
      AuxMethods.readAndPrepareDF(
        positivePath,
        positivePath
      ),
      AuxMethods.readAndPrepareDF(
        negativePath,
        negativePath
      )
    )

    val instanceClassifier = mode match {
        // Choose the greatest NLDD valued hypothesis
      case ClassificationMode.MAX =>
        udf(EMDDInstanceClassify(Array(results.maxBy(_._3)), indexMapper, threshold) _)
      case ClassificationMode.AVG =>
        // Average the probabilities of the hypotheses
        udf(EMDDInstanceClassify(results, indexMapper, threshold) _)
      case _ =>
        // Choose the smallest NLDD valued hypothesis
        udf(EMDDInstanceClassify(Array(results.minBy(_._3)), indexMapper, threshold) _)
    }

    // Classify by averaging
    (
      dfPos
        .withColumn("PROBABILITY", instanceClassifier(struct(dfPos.columns.map(dfPos(_)): _*)))
        .groupBy(bagIdColumnName)
        .agg(max("PROBABILITY").as("PREDICTION")),
      dfNeg
        .withColumn("PROBABILITY", instanceClassifier(struct(dfNeg.columns.map(dfNeg(_)): _*)))
        .groupBy(bagIdColumnName)
        .agg(max("PROBABILITY").as("PREDICTION"))
    )
  }


  /**
    * Prints the results obtained by running the EMDD algorithm
    *
    * @param results the results to be printed
    */
  def printResult(results : Array[(Array[Double], Array[Double], Double)]) : Unit = {

    results
      .zipWithIndex
      .foreach(tuple => {

        println("Hypothesis number " + tuple._2 + ": [" + tuple._1._1.mkString(", ") + "]")
        println("Scales number " + tuple._2 + ": [" + tuple._1._2.mkString(", ") + "]")
        println("NLDD value number " + tuple._2 + ": " + tuple._1._3)

        println("\n")

    })

  }

  /**
    * Write the result of the EMDD to a file
    *
    * @param results the EMDD results
    * @param path the path to the file
    */
  def writeResultToFile(results : Array[(Array[Double], Array[Double], Double)],
                        path : String) : Unit = {

    val writer : PrintWriter = new PrintWriter(new File(path))

    //writer.println(results.length)

    results.foreach(result => {

        writer.println(result._1.mkString(","))
        writer.println(result._2.mkString(","))
        writer.println(result._3)

      }
    )

    writer.close()
  }

  /**
    * Reads the EMDD results from a file
    *
    * @param path the path to the file
    * @return the results of the EMDD algorithm from the given file
    */
  def readResultFromFile(path : String) : Array[(Array[Double], Array[Double], Double)] = {

    val lines = Source.fromFile(path).getLines().toList

    var result : Array[(Array[Double], Array[Double], Double)] = Array[(Array[Double], Array[Double], Double)]()

    for (index <- lines.indices by 3) {
      val hypothesis : Array[Double] = lines(index).split(",").map(_.toDouble)
      val scales : Array[Double] = lines(index + 1).split(",").map(_.toDouble)
      val value : Double = lines(index + 2).toDouble

      result = result :+ (hypothesis, scales, value)
    }

    // Return the results
    result
  }

  /**
    * Generates a map between the relevant features and the hypothesis
    *
    * @param path to the file containing the relevant features which will be transformed to an index mapper
    */
  def getIndexMapper(path : String) : Map[Int, String] = {

    val lines = Source.fromFile(path).getLines()

    var indexMapper : Map[Int, String] = Map[Int, String]()

    for (zipped <- lines.zipWithIndex) {

      indexMapper = indexMapper + (zipped._2 -> zipped._1)

    }

    //Return the map
    indexMapper
  }

}